import json


def hello(event, context):
    query_params = event['queryStringParameters']
    body = {
        "message": "Go Serverless v2.0! Your function executed successfully!",
        "input": event,
        "customOutput": query_params
    }

    response = {"statusCode": 200, "body": json.dumps(body)}

    return response

    # Use this code if you don't use the http event with the LAMBDA-PROXY
    # integration
    """
    return {
        "message": "Go Serverless v1.0! Your function executed successfully!",
        "event": event
    }
    """

def cami(event, context):
    # uuid = event['queryStringParameters']['uuid']
    body = {
        "event": event,
        "uuid": 'NA'
    }
    response = {"statusCode": 200, "body": json.dumps(body)}

    return response
